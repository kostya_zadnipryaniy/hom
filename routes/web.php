<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/article/hate', 'ArticleController@hate')->name('article.hate');
Route::get('/article/love', 'ArticleController@love')->name('article.love');
Route::get('/article/show/{id}', 'ArticleController@show')->name('article.show');

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'auth'], function () {
	// Route::get('pages', 'PageController@index')->name('pages.index');

	// Route::get('pages/{id}', 'PageController@show')->name('pages.show');

	Route::resource('pages', 'PageController');
	Route::resource('photos', 'PhotoController');
	Route::resource('articles', 'ArticleController');
	Route::get('parameters', 'ParametersController@show')->name('parameters.show');
	Route::get('parameters/edit', 'ParametersController@edit')->name('parameters.edit');
	Route::put('parameters', 'ParametersController@update')->name('parameters.update');

});

Route::get('/hate', function () {
    return view('hate');
})->name('hate');

Route::get('/love', function () {
    return view('love');
})->name('love');

Route::get('/AddPhoto', function () {
    return view('Addphoto');
})->name('AddPhoto');

Route::get('/AddComment', function () {
    return view('AddComment');
})->name('AddComment');

Route::get('/AddNews', function () {
    return view('AddNews');
})->name('AddNews');

Route::get('/RewievPhoto', function () {
    return view('Rewievphoto');
})->name('RewievPhoto');

Route::get('/RewievComment', function () {
    return view('RewievComment');
})->name('RewievComment');

Route::get('/RewievNews', function () {
    return view('RewievNews');
})->name('RewievNews');