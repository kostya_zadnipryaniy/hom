<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>hom</title>
        <!-- Styles -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300&display=swap" rel="stylesheet">
        <style>
            html, body {
                background: url('images/fregat.png');
                background-size: 100% 100%;
                font-family: 'Arial', sans-serif;
                font-weight: 200;
                height: 100vh;
                width: 100vw;
                margin: 0;
                padding: 0;
                border: 0;
                font-size: 16px;
            }



            .position-ref {
                position: relative;
            }

            .top-right {
                position: fixed;
                right: 10px;
                top: 18px;
                z-index: 100;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: .815rem;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }
            .choice-container {
                display: flex;
            }
            .main-choice {
                position: relative;
                width: 50%;
                height: 100vh;

            }
            .choice-text {
                position: absolute;
                font-family: 'Open Sans Condensed', ;
                font-size: 3rem;
                text-shadow: 1px 1px 2px black, 0 0 1em red;
            }
            .hate > .choice-text {
                top: 10vh;
                left: 3rem;
            }
            .love > .choice-text {
                bottom: 10vh;
                right: 3rem;
            }
            @media screen and (max-width: 800px) {
                .choice-text {
                    font-size: 2rem;
                }
            }
            @media screen and (max-width: 600px) {
                .choice-text {
                    font-size: 1rem;
                }
            }
        </style>
    </head>
    <body>

            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ route('home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif
            <div class="choice-container">
                <a href="{{ route('hate') }}" class="main-choice hate">
                    <span class="choice-text">I hate Kherson</span>
                </a>
                <a href="{{ route('love') }}" class="main-choice love">
                    <span class="choice-text">I love Kherson</span>
                </a>
            </div>

    </body>
</html>
