<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>love</title>
        <!-- Styles -->
        <style>
            html, body {
                background: url('images/love.png');
                background-size: 100% 100%;
                font-family: 'Arial', sans-serif;
                font-weight: 200;
                height: 100vh;
                width: 100vw;
                margin: 0;
                padding: 0;
                border: 0;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: fixed;
                right: 10px;
                top: 18px;
                z-index: 100;
            }

            .title {
                font-size: 84px;
            }

             .unit {
                display: inline-block;
                width: 49.8vw;
                height: 100vh;
                margin: 0;
                padding: 0;
                border: 0;
                z-index: 1000;


            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }
        </style>
    </head>
    <body>

            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <a href="{{ url('/hate') }}"><div class="unit"></div></a>
            <a href="{{ url('/love') }}"><div class="unit"></div></a>

<!--             <div class="content">
                <div class="title m-b-md">
                   hom
                </div>

                <div class="links">
                    <a href="https://laravel.com/docs">Docs</a>
                    <a href="https://laracasts.com">Laracasts</a>
                    <a href="https://laravel-news.com">News</a>
                    <a href="https://blog.laravel.com">Blog</a>
                    <a href="https://nova.laravel.com">Nova</a>
                    <a href="https://forge.laravel.com">Forge</a>
                    <a href="https://github.com/laravel/laravel">GitHub</a>
                </div>
            </div>
        </div> -->
    </body>
</html>
