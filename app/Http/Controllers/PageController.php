<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
  public function show($id)
	{
		$data = Page::where('id', $id)->firstOrFail();
		return view(['page' => $data]);
	}  //
}
