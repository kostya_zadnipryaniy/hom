<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request\PhotoRequest;
use App\Http\Controllers\Controller;
use App\photo;

class PhotoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Photo::all();
        return view('admin.photo.index', ['photos' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('admin.photo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PhotoRequest $request)
    {
       $photo = new Photo();
        $photo->fill($request->all());
        $photo->save();
        return redirect()->route('photos.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Photo::where('id', $id)->firstOrFail();
        return view('admin.photo.show', ['photo' => $data]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Photo::where('id', $id)->firstOrFail();
        return view('admin.photo.edit', ['photo' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PhotoRequest $request, $id)
    {
         $photo = Photo::where('id', $id)->firstOrFail();
        $photo->fill($request->all());
        $photo->save();
        return redirect()->route('photos.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $photo = Photo::where('id', $id)->firstOrFail();
        $photo->delete();
        return redirect()->route('photos.index');
    }
}

