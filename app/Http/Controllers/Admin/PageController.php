<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request\PageRequest;
use App\Http\Controllers\Controller;
use App\Page;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data = Page::all();
        return view('admin.page.index', ['pages' => $data]);
>>>>>>> d3636b0cc41c3678c2e3020d7147fa318e63cba2
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('admin.page.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PageRequest $request)
    {
       $page = new Page();
        $page->fill($request->all());
        $page->save();
        return redirect()->route('pages.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Page::where('id', $id)->firstOrFail();
        return view('admin.page.show', ['page' => $data]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Page::where('id', $id)->firstOrFail();
        return view('admin.page.edit', ['page' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PageRequest $request, $id)
    {
         $page = Page::where('id', $id)->firstOrFail();
        $page->fill($request->all());
        $page->save();
        return redirect()->route('pages.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $page = Page::where('id', $id)->firstOrFail();
        $page->delete();
        return redirect()->route('pages.index');
    }
}
