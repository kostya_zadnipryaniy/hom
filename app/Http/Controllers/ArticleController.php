<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;

class ArticleController extends Controller
{
	public function hate()
	{
		$data = Article::where('hate', true)->all();
		return view('article.hate', ['articles' => $data]);
	}
	public function love()
	{
		$data = Article::where('hate', false)->all();
		return view('article.love', ['articles' => $data]);
	}
	public function show($id)
	{
		$data = Article::where('id', $id)->firstOrFail();
		return view('article.show', ['article' => $data]);
	}
}
