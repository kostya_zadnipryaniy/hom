<?php

use Illuminate\Database\Seeder;

class PhotosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('pages')->insert([
			[
	        	'src' => 'https://upload.wikimedia.org/wikipedia/commons/5/51/Kristall_Stadium_in_Kherson.jpg',
				'name' =>'Kristal studium',
				'author'  => 'homer',
	        ],
			[
	            'src' => 'https://www.cruisemapper.com/images/ports/333-a898bd2672a4.jpg',
				'name' =>'Dnipro river',
				'author'  => 'homer',
	        ],
			[
	           'src' => 'https://ukrainetrek.com/images/kherson-ukraine-city-views-18.jpg',
				'name' =>'Фрегат',
				'author'  => 'homer',
	        ]
   ] }
}
