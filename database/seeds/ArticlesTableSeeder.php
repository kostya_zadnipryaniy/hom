<?php

use Illuminate\Database\Seeder;

class ArticlesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
   public function run()
    {
		DB::table('Articles')->insert([
			[
	            'resource' => 'home',
	            'title' => 'Статьи',
	            'description' => 'О городе',
	            'keywords' => 'Херсон',
	            'image' => 'https://khersondaily.com/images/news/original/979e54e1834ba06e01827e3d16f14155.jpg',
	            'name' => 'Gagarina str',
	            'content' => 'Проезжающий транспорт выбрасывает мусор не выходя из машины',
	            'hate' => true,
	        ],
			[
	            'resource' => 'home',
	            'title' => 'Статьи',
	            'description' => 'О городе',
	            'keywords' => 'Херсон',
	            'image' => 'http://marusnews.com/wp-content/uploads/2018/03/Avtostantsiya-3-1.jpg',
	            'name' => 'Центральный рынок',
	            'content' => 'Самогон 50 грн/л',
	            'hate' => true,
	        ],[
	            'resource' => 'home',
	            'title' => 'Статьи',
	            'description' => 'О городе',
	            'keywords' => 'Херсон',
	            'image' => 'http://favoritekherson.co/uploads/posts/2018-02/1517735958_sinyavina.jpg',
	            'name' => 'Рынок Сенявина',
	            'content' => 'Все для покупателя. Купайся, нехочу!',
	            'hate' => true,
	        ]
	    ]);
    }
}
